package selectonelistbox.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.input.RichInputText;

public class SelectOneListBoxBean {
    //Component Binding For Input Text
    private RichInputText itBind;

    public void setItBind(RichInputText itBind) {
        this.itBind = itBind;
    }

    public RichInputText getItBind() {
        return itBind;
    }

    //String variable to hold selectOneListBox value
    private String selectedVal;

    public void setSelectedVal(String selectedVal) {
        this.selectedVal = selectedVal;
    }

    public String getSelectedVal() {
        return selectedVal;
    }

    public SelectOneListBoxBean() {
    }
    //List DataStrucutre to populate values in selectOneListBox
    List<SelectItem> customList = new ArrayList<SelectItem>();

    public void setCustomList(List<SelectItem> customList) {
        this.customList = customList;
    }

    public List<SelectItem> getCustomList() {
        return customList;
    }

    /**Method to add Record in List
     * @param actionEvent
     */
    public void addValueInList(ActionEvent actionEvent) {
        //Get value from inputText using component binding
        if (itBind.getValue() != null) {
            //Add value in List
            customList.add(new SelectItem(itBind.getValue().toString()));
        }
    }

    /**Method that check for selected value in List
     * @param items
     * @param value
     * @return
     */
    public SelectItem getItem(List<SelectItem> items, String value) {
        for (SelectItem si : items) {
            System.out.println(si.getValue());
            if (si.getValue().toString().equalsIgnoreCase(value)) {
                return si;
            }
        }
        return null;

    }

    /**Method to delete selected record from List
     * @param actionEvent
     */
    public void deleteSelectedValue(ActionEvent actionEvent) {
        if (selectedVal != null) {
            //Find and delete selected item from List
            customList.remove(getItem(customList, selectedVal));
        }
    }
}

//